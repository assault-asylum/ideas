# ideas

##### Cosmetic:
- [ ] Vehicles - hats and shit (have a user side disable for competitive players?)
- [ ] Players - hats and skins (have a user side disable hats for competitive players?)
- [ ] more competitive skins that allow for  better sneaking?

##### Game Modes:
- [ ] RACE - Badger Kart Double Dash
- [ ] FIESTA - Mario Party (a better one please... Shorter... More ASS and RACE objectives. Too)
- [ ] FIESTA - Scorp Golf (biorifle)
- [ ] RACE - Return of Trials?
- [ ] ONS - Titan Mode (influenced from BF2142 Titan Mode)
- [ ] AS - Multi way assault 

##### Server Modifications:
- [ ] per game mode: map rotation. Have up to X pinned maps to always have, then an extra 10. Rotates daily/bi-weekly/weekly. Perhaps have it say "Daily pick!" Or "Map of the Week" or something
- [ ] game mode rotation. Ass and Race are pinned. vctf, ons,fiesta and other modes are rotated on a weekly basis.
- [ ] text chat while loading map? Is it possible? Cause you can type
- [ ] Remove randomized respawn timer
- [x] vote skip map

###### Map Packages
- [x] Vehicles - TurboVehicles
- [ ] Vehicles - Flying vehicle with ability to grab/lift other vehicles
- [ ] Vehicles - Rally Truck (passenger weapon grenade launcher? Rocket launcher) per wheel check floor material?
- [ ] Vehicles - indesctructotank
- [ ] Vehicles - motorstorm?
- [ ] Vehicles - amazing MW:LL VTOL
- [ ] Vehicles - MechWarrior walkers
- [ ] 

###### Maps
- [x] Ice Vindi


##### Misc.:
- [ ] Organize all the best UT fixes in one place
- [ ] A tiny temple near some spawns dedicated to Fubar, barely big enough to fit 1 player. It has a big weapon (e.g. redeemer/shockrifle/rocketlauncher) behind a door that only opens after x minutes. Like on lego wars, it adds a new opportunity to make some progress if the team is stuck on one objective for too long. Might save a couple of ragequits.
